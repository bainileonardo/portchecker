import java.net.InetSocketAddress;
import java.net.Socket;

public class PortCheckerMain {

	public static void main(String[] args) {

		System.out.println(ejecutarPrueba(args[0], args[1])); 

	}
	public static boolean ejecutarPrueba(String ip,String puerto) {
		int int_puerto=Integer.parseInt(puerto);
		boolean portAvailable;
		
		int delay = 1000; 
		try {
			Socket socket = new Socket();

			socket.connect(new InetSocketAddress(ip, int_puerto), delay);
			portAvailable = socket.isConnected();
			socket.close();
			
		}
		catch (Exception e) {
			portAvailable = false;
			
		}

		return portAvailable;
	}

}
